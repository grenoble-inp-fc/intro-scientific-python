The goal is to install a local package using the [snippet code](https://gitlab.com/grenoble-inp-fc/intro-scientific-python/snippets/1839217)

A good starting point is 
1. [building a package](https://packaging.python.org/tutorials/packaging-projects/)
1. [installing a package](https://packaging.python.org/tutorials/installing-packages/)

The structure of your package should be:
package name : myutils
containing the modules : functions (which is the code snippet)

Hence, importing the decorator goes like
```python
from myutils.function import argcheck

Make sure to create a new environment in conda
```bash
conda create -n firstbuild python=3
conda activate firstbuild
conda install wheel
```
any new environment comes with `setuptools` and `pip` installed, you may want to install `wheels`
