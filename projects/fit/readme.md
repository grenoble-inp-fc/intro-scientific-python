# Inference for the Voigt distribution

In this exercice you will integrate most of your skills throughout different steps of inference, leading to a _maximim a posteriori_ estimate of the parameters.

## 1. Plotting distributions

1. Use `matplotlib.pyplot` and `scipy.stats` to draw some probability density functions for the normal random variable and the Lorentz distribution (Cauchy random variable).

1. write the Voigt pdf using the Faddeeva function `wofz` from the module `scipy.special` (have a peak at the wikipedia page on the [Voigt](https://en.wikipedia.org/wiki/Voigt_profile) distribution)

## 2. Generating random variables

1. Use the random variable generators from `scipy.stats` to generate random variables from the normal and Cauchy random variable

1. Generate a random variable from the Voigt distribution using the sum of independent Cauchy$(x_0, \gamma)$ and Gaussian$(0, \sigma^2)$ random variables.

1. For the random numbers, plot the histogram and compare to the shape of the pdf.

## 3. Building your first package

> We will use an __object-oriented interface__ to the random number generator that is contained in your first __package__. 

A good starting point for building your first package is 
1. [building a package](https://packaging.python.org/tutorials/packaging-projects/)
1. [installing a package](https://packaging.python.org/tutorials/installing-packages/)

The structure of your package should be:
* package name : `mystats`
* containing the module : `voigt` (which contains your class definition)

Hence, importing `voigt` goes like
```python
from mystats.voigt import Voigt
```

Make sure to create a new environment in conda
```bash
conda create -n firstbuild python=3
conda activate firstbuild
```
any new environment comes with `setuptools` and `pip` installed, you may also want to install `wheels` with ```conda install wheel```.


## 4. Evaluating and maximising the likelihood function

1. write the log-likelihood function of the Voigt distribution

2. maximise the (log-)likelihood by using the `scipy.optimize` module

What is the inconvenience of this method?

## 5. Generating a random variable using Metropolis--Hastings (Markov chain Monte Carlo)

Generating variables from distribution for which it is hard to sample directly can be done using a chain of random variables (with correlations). An example is the class of acceptance--reject methods. Here we will use the Metropolis--Hastings algorithm where we generate a new sample $x^{\prime}$ for which the probability can be evaluated up to a multiplicative factor, say $f(x^{\prime})$. Comparing to the probability of the old sample $x$, $f(x)$, we accept $x^{\prime}$ if the ratio $f(x^{\prime})/f(x)$ is larger than a uniform random variable $U$, otherwise we accept $x$ again.

Let us sample from the half--circle law centered at $0$ and with radius $r=1$ (domain of the variable $[-r, r]$).

1. Let us use a dominated measure (a rectangle). Generate uniformly some points in the rectangle, retain only those points that are in the circle and store their absiscae.

1. Let us step from the current values of the parameters $x$ to the next $x^{\prime}$ using a Gaussian distribution with location $x$ and scale $0.1$, using the Metropolis--Hasting acceptance-reject rule to sample. If a point is out of the domain, the probability is zero, hence we will not accept it and come back to the latest point (which is hence stored twice).

## 6. Evaluating the _a posteriori_ distribution over the parameters

Generate 100 samples from a Voigt distribution with some user-defined parameter values.

1. Expressing the posterior of the Voigt parameters using exponential prior distributions for $\sigma$ and $\gamma$, and a normal prior distribution for $x_0$, sample from the posterior.

1. Generate parameters (samples) according to the posterior distribution and store the parameter values and their posterior probability (up to a scaling factor). We'll generate $10^4 + 10^3$ samples from which we will discard the first $10^3$, due to the _burn-in_ of the algorithm.

2. Give the MAP (maximum a posteriori) estimate of the parameters, the mean of the _a posteriori_ random variables, the covariance, and the correlation matrix of the _a posteriori_ random variables.

> Use `seaborn.jointplot` to visualise the joint posterior distribution of $\sigma$ and $\gamma$, $\sigma$ and $x_0$, and $\gamma$ and $x_0$.

## 7. Repeat with different initialisation points

1. Use a `for`-loop to estimate the parameters that describe best the data for at least 10 runs starting from different values.

1. Use the `multiprocessing` module from the standard library to parallelise the `for`-loops.

> One must pay attention here to use a single function that is called, with a single parameter, and `numpy` should define a separate `seed` for each of the processes. See also [random seeds and multiprocessing](https://stackoverflow.com/questions/6914240/multiprocessing-pool-seems-to-work-in-windows-but-not-in-ubuntu).

You could use the magic `%%timeit` in _jupyter notebook_ to compare performances.