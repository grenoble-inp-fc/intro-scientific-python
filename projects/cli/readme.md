The goal is to create a command line interface to a python program that scans a given path for images. 

Use the standard library package `argparse` for the cli (command line interface) and the `pil` package for its capacities of reading image metadata without reading the whole image in memory (hence, the result should not depend on the extension, but only on the metadata). 

Optional arguments:
The directory to scan (defaults to the directory in which the program is launched)

The depth of the path tree could be given as an optional argument, where
-1 : scan all child directories,
0  : only the directory itself
1  : the directory itself and all children (but not any deeper)
etc.

Some hints:
* look at the Exception that is raised when `pil` is asked to open a non-image file
* make sure the user knows how to use your code by providing the necessary documentation through `help` (see `argparse`)
* start simple by only scanning the local directory, then scanning a directory specified by the user, and only then a scanning at multiple depths should be implemented
