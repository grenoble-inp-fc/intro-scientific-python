# Introduction to Scientific Python
_by Grenoble INP - Formation Continue_

This repository contains the course material for the introductory course to scientific python, its main objectives are
* to make you familiar with the python language: its philosophy, its specifics
* to give you an introduction to the scientific programming environment offered by the python community: numpy, scipy, pandas, plot.ly, matplotlib, &hellip;

This is meant to be a 3-day cours, which is organised as follows
#### Day 1
1. Introduction to the Python community structure and its philosophy
1. The basics of the python programming language
#### Day 2
3. Introduction to the numerical programming package `numpy`
3. Introduction to data visualisation with `matplotlib` and `plot.ly`
#### Day 3
5. Implementing a package or a module yourself
5. Project work
