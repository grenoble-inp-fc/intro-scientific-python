{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 3. Flow control\n",
    "\n",
    "1. [introduction](#introduction)\n",
    "1. [Boolean logic](#boolean_logic)\n",
    "1. [Exceptions](#exception_handling)\n",
    "1. [iterators](#iterators)\n",
    "1. [Break-continue-pass](#break-cont-pass)\n",
    "1. Exercise: [prime sift](#primes)\n",
    "\n",
    "## 3.1 Introduction <a name=\"introduction\" />\n",
    "\n",
    "### Motivating example: solving the heat equation on a circular body\n",
    "\n",
    "The expression of the heat diffusion over a circular body at initial temperature $\\forall x,\\,T(x, 0)=0$ of radius $R$ when a single point is heated at time $t=0$, is given by\n",
    "\n",
    "$$T(x, t) = 1 + 2\\sum_{n=1}^{+\\infty}\\mathrm e^{-\\frac{n^2}{R^2}t}\\cos\\left(\\frac{n}{R}x\\right)$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# import a plotting library for showing the results\n",
    "from plotly.offline import iplot, init_notebook_mode\n",
    "from plotly import graph_objs as go\n",
    "from math import pi, exp, cos\n",
    "init_notebook_mode(connected=True)\n",
    "\n",
    "N_POINTS = 1000\n",
    "R = 1/pi\n",
    "t = 1/10 # change freely for any t >= 0\n",
    "dE = 1e-2\n",
    "\n",
    "layout = go.Layout(title='Spatial distribution of temperature',\n",
    "                   xaxis=go.layout.XAxis(title='x'),\n",
    "                   yaxis=go.layout.YAxis(title='T'))\n",
    "X = [2 * pi * k * R/N_POINTS - pi * R for k in range(N_POINTS)]\n",
    "\n",
    "n = 0\n",
    "T = [1] * N_POINTS\n",
    "deltaE = 1\n",
    "while True:\n",
    "    try:\n",
    "        n += 1\n",
    "        deltaE = exp(-(2*n+1) / R**2 * t) # relative contribution of energies <u[n+1]²>/<u[n]²>\n",
    "        if deltaE < dE:\n",
    "            raise StopIteration\n",
    "    except StopIteration:\n",
    "        break\n",
    "    # only compute update if significant enough\n",
    "    T = [T[k] + 2 * exp(-(n/R) ** 2 * t) * cos(n * x / R) for k, x in enumerate(X)]\n",
    "    \n",
    "\n",
    "data = [go.Scatter(x=X, y=T)]\n",
    "fig = go.Figure(data, layout)\n",
    "fig['layout']['title'] = 'Spatial distribution of temperature at t={} using {} terms'.format(t, n)\n",
    "fig['layout']['yaxis']['range'] = [0, max(T)*1.05]\n",
    "iplot(fig)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### key ingredients for flow control\n",
    "\n",
    "1. an _infinite_ loop: repeats until conditional rerouting tells otherwise\n",
    "1. conditional rerouting: indicates which program path is executed, given a condition is (un)satisfied\n",
    "1. a final condition: indicates when to terminate the iterations\n",
    "\n",
    "The **infinite loop** is created by \n",
    "```python\n",
    "while True:\n",
    "    <statements> # obligatory 4-space indentation!\n",
    "```\n",
    "\n",
    "Python has two ways for **conditional rerouting**\n",
    "* `if`/`elif`/`else` with conditional statements &rightarrow; we need to evaluate _boolean expressions_\n",
    "* throw an `Exception` &rightarrow; we need to handle these properly (`try` &hellip; `except` &hellip; `finally`)\n",
    "\n",
    "A **final condition** can be\n",
    "* an exception occurs, e.g., `Exception:StopIteration`\n",
    "* a `break`, `continue`, or `return` statement appears\n",
    "\n",
    "\n",
    "Kleening up\n",
    "* if no other condition was satisfied, use `else` for the all-but-the-former condition\n",
    "* `finally` which executes whatsoever happened before"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3.2 conditional rerouting with boolean logic <a name=\"boolean_logic\"/>\n",
    "\n",
    "### Getting acquainted with boolean expressions in python\n",
    "\n",
    "#### basic boolean values and operators\n",
    "\n",
    "* 2 boolean values : `True` and `False`\n",
    "\n",
    "* comparison operators : `==`,&ensp; `>`,&ensp; `<`,&ensp; `<=`,&ensp; `>=`,&ensp; `!=`,&ensp; `is`\n",
    "\n",
    "* negation: `not`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(True, False, not True, not False)\n",
    "print(False == False, False!=True)\n",
    "print((not False==True) == (False!=True))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(1 < 2 < 3)  # equivalent to 1 < 2 and 2 < 3\n",
    "print(3 != 1 < 2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### comparison operators: _to be or not to be_\n",
    "\n",
    "| operator | meaning |\n",
    "|:-------- |:----|\n",
    "| `==` | __similar__ _or_ equal in __value__|\n",
    "| `is` | __identical__ _or_ same __object__ |\n",
    "\n",
    "_For fancier string layout when printing to stdout, see [input output](https://docs.python.org/3.6/tutorial/inputoutput.html)_"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(0 == 0.)  # this is duck typing! only values are important\n",
    "print(0 is 0.)  # identity checks whether this is truly one and the same"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = 256\n",
    "b = 256\n",
    "print(a is b)\n",
    "a = 257\n",
    "b = 257\n",
    "print(a is b)   # prefer the use of '==' over 'is', unless you compare to None (or for object identity, see later)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = \"Hello world!\"\n",
    "y = \"Hello world!\"\n",
    "print(x == y)   # easy string comparison\n",
    "print(x is y)\n",
    "x = y = \"Hello world!\"  # truly pointing to the same string!\n",
    "print(x is y)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# consequence\n",
    "x = [1, 2]\n",
    "y = [1, 2]\n",
    "y[1] = 4\n",
    "print(x)\n",
    "\n",
    "x = [1, 2]\n",
    "y = x      # looks similar as the above\n",
    "y[1] = 4   # ... lists are mutable, y is merely a reference to x!\n",
    "print(x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### combining logical statements through operators\n",
    "\n",
    "* joining statements &rightarrow; logical operations\n",
    "\n",
    "    * `and`, `or`\n",
    "    * bitwise: `&` (and), `|` (or), `^` (xor)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(True and False)\n",
    "print(True or True and False)  # operator precedence!\n",
    "print(not True or True)        # operator precedence!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# cheating : use binary xor on booleans to define a logical xor\n",
    "print(True ^ True, True ^ False, False ^ True, False ^ False)\n",
    "\n",
    "print(bool(0) ^ bool(6))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(12 ^ 10)          # binary xor\n",
    "print(bin(12))\n",
    "print(bin(10))\n",
    "print('{:#06b}'.format(12 ^ 10))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### implicit boolean logic\n",
    "\n",
    "* predicates are used in flow control `if`, `while`\n",
    "\n",
    "* all following statements evaluate to `True`\n",
    "\n",
    "  ```python\n",
    "  not 0\n",
    "  \"non-empty string\"\n",
    "  not \"\"\n",
    "  bool(2)\n",
    "  bool([1, 2])\n",
    "  not bool([])\n",
    "  ```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### trickier behaviour when not dealing with logical expressions\n",
    "\n",
    "Careful when you want to shortcut the casting to a boolean"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(1 and 2)\n",
    "print(bool(1) and bool(2))\n",
    "print(True and \"print me\")  # if the first statement evaluates to True print second\n",
    "print(False and \"print me\") # if the first statement evaluates to False print False\n",
    "print(\"print me\" and True)  # non-commutative!\n",
    "print(\"print me\" and False) "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### compound statements\n",
    "\n",
    "* compound statement = header + suite\n",
    "\n",
    "* separator `:`\n",
    "\n",
    "* suite &rightarrow; __4 space__ indentation!\n",
    "\n",
    "    ```python\n",
    "    if True:\n",
    "        print(\"execute me\")\n",
    "    ```\n",
    "\n",
    "  * header:<br />\n",
    "    &bbrk;&bbrk;&bbrk;&bbrk;suite\n",
    "    \n",
    "#### `if`/`elif`/`else` statement"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "if True:\n",
    "    print('execute me')\n",
    "if False:\n",
    "    print('do not execute me')\n",
    "if not False:\n",
    "    print('double negation is an affirmation')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = 1\n",
    "if x is 1:\n",
    "    print('x identifies with 1')\n",
    "elif x == 1: # Auch ! although True this does not get evaluated, since the above \"x is 1\" was True already\n",
    "    print('the value of x is 1')\n",
    "else:\n",
    "    print('x has nothing to do with 1')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### one-liner conditional statement"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = 1 if 2 > 3 else 4  # read it as if it were English, and you'll guess the answer\n",
    "print(x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "__Exercise__\n",
    "\n",
    "Implement the absolute value of `x` as a conditional assignment"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# rectifying linear unit or absolute value\n",
    "x = -2\n",
    "# put your conditional one-liner here\n",
    "\n",
    "print(x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### `while` statement\n",
    "\n",
    "_HINT_: you can interrupt kernel execution with <kbd>Ctrl</kbd>+<kbd>C</kbd> or interupt the kernel by <kbd>ESC</kbd>+<kbd>I</kbd> or use the menu _Kernel_ &rightarrow; _Interrupt_"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "while True: # infinite loop (no final condition), no user feedback DO NOT EXECUTE\n",
    "    pass  # pass is a dummy statement, can be used whenever a suite is needed syntactically"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Exercice: power-2\n",
    "Compute the smallest power of two that is greater than $10^9$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# ... to complete"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Exercise: buffer\n",
    "\n",
    "* implement a _last in first out_ (LIFO) buffer\n",
    "\n",
    "* implement a _first in first out_ (FIFO) buffer<br />\n",
    "  you might want to have a look at [data structures](https://docs.python.org/3.6/tutorial/datastructures.html)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# fill the elements of the list using a while loop\n",
    "# then read them back out using a LIFO buffer"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# fill the elements of the list using a while loop\n",
    "# then read them back out using a FIFO buffer"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A way to interrupt the __while__ loop is to have an error (__Exception__ in python) popping up"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3.3 Exception handling <a name=\"exception_handling\" />\n",
    "see [Exceptions](https://docs.python.org/3/tutorial/errors.html)\n",
    "\n",
    "### 3.3.1 the `try` &#8230; `except` &#8230; `finally` triplet\n",
    "\n",
    "* Exception (\"error\") &rightarrow; if not handled, code execution is stopped\n",
    "\n",
    "* handling Exceptions &rightarrow; `try` &#8230; `except` (&#8230; `finally`)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "raise Exception  ## raise an Exception, here"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are a large number of more specific Exceptions [readily available](https://docs.python.org/3/library/exceptions.html). It is good practice to use these if they apply and only create your own when you cannot find an appropriate Exception in this list."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = 1\n",
    "b = 0\n",
    "a / b  # raises an Exception, since undetermined value\n",
    "    \n",
    "# if the exception is raised, code is halted, no execution of the next line\n",
    "print(\"I'm Always present at output, whatever exception raised\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Handle the exception with a `try` &hellip; `except` statement"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = 1\n",
    "b = 0\n",
    "try:\n",
    "    a = 1\n",
    "    b = 0\n",
    "    c = a / b\n",
    "except:\n",
    "    print(\"Exception was raised, but which one ?\")\n",
    "    \n",
    "# if the exception is handled rightly, we can go on just as nothing happened\n",
    "print(\"I'm Always present at output, whatever exception raised\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = 1\n",
    "b = 0\n",
    "try:\n",
    "    a = 1\n",
    "    b = 0\n",
    "    c = a / b\n",
    "except:\n",
    "    print(\"Exception was raised, but which one ?\")\n",
    "    raise  # we could raise the Exception again, hoping some other part in the program will handle it\n",
    "    \n",
    "# if the exception is raised, code is halted, no execution of the next line\n",
    "print(\"I'm Always present at output, whatever exception raised\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = 1\n",
    "b = 0\n",
    "try:\n",
    "    a = 1\n",
    "    b = 0\n",
    "    a / b\n",
    "except Exception as e:   # we can label the exception to reuse later\n",
    "    print(\"First print me, then raise \\\"{}\\\" exception again\".format(e))\n",
    "    raise                # raises last exception again"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The __finally__ header: allows to safely execute a suite, even after an error is raised"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = 1\n",
    "b = 0\n",
    "try:\n",
    "    a = 1\n",
    "    b = 0\n",
    "    c = a / b\n",
    "except:\n",
    "    print(\"Exception was raised, but which one ?\")\n",
    "    raise\n",
    "finally:    \n",
    "    print(\"I'm Always present at output, whatever exception raised\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = 1\n",
    "b = 0\n",
    "try:\n",
    "    a = 1\n",
    "    b = 0\n",
    "    a / b\n",
    "except ZeroDivisionError as e: # only division-by-zero errors\n",
    "    print(\"I'm printed when a division by zero occurs: \\\"{}\\\"\".format(e))\n",
    "    raise                      # raises last exception again\n",
    "except Exception as e: # all other errors\n",
    "    print(\"I'm printed for any other exception: here \\\"{}\\\"\".format(e))\n",
    "    raise\n",
    "finally:\n",
    "    print(\"I'm Always present at output, whatever exception raised\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = 1\n",
    "b = 0\n",
    "try:\n",
    "    a = 1\n",
    "    b = 0\n",
    "    a[0] # try \"a[0]\" instead\n",
    "except ZeroDivisionError as e: # only division-by-zero errors\n",
    "    print(\"I'm printed when a division by zero occurs: \\\"{}\\\"\".format(e))\n",
    "    raise                      # raises last exception again\n",
    "except Exception as e: # all other errors\n",
    "    print(\"I'm printed for any other exception: here \\\"{}\\\"\".format(e))\n",
    "    raise\n",
    "finally:\n",
    "    print(\"I'm Always present at output, whatever exception raised\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "__Example__ \n",
    "\n",
    "Get the largest power of 2 lower than or equal to $10^5$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = 1\n",
    "while True:\n",
    "    y = 2 * x\n",
    "    if y > 1e5:\n",
    "        raise StopIteration  # iteration stopped, but so did the whole program!\n",
    "    else:\n",
    "        x = y\n",
    "\n",
    "print(\"Final value x = {}\".format(x))  # this is never reached"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = 1\n",
    "while True:\n",
    "    try:\n",
    "        y = x * 2\n",
    "        if y > 1e5:\n",
    "            raise StopIteration\n",
    "        else:\n",
    "            x = y\n",
    "            print(\"Current value x = {}\".format(x))\n",
    "    except StopIteration:\n",
    "        break # breaks here "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = 1\n",
    "while True:\n",
    "    try:\n",
    "        y = 2 * x\n",
    "        if y > 1e5:\n",
    "            raise StopIteration\n",
    "        else:\n",
    "            x = y\n",
    "    except StopIteration:\n",
    "        break # breaks here\n",
    "    finally:  # executes 'finally' suite after each iteration, even when the Exception is raised !\n",
    "        print(\"Current value x = {}\".format(x))    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "__Exercise__\n",
    "\n",
    "Inspect the error of the following statement and implement a way to secure that an integer will be given by the user (ask the user to repeat until an integer is given)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n = int(input(\"Give an integer value: \"))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Exercise: your self-designed for-loop\n",
    "\n",
    "Define a for-loop using an infinite `while` and the `StopIteration` exception. The for-loop should print the 26 letters of the alphabet one by one on separate lines"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "alphabet = 'abcdefghijklmnopqrstuvwxyz'\n",
    "# to complete"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3.4 iterators <a name=\"iterators\"/>\n",
    "\n",
    "* __iterable__: returns its members one after another\n",
    "\n",
    "    * `__getitem__()`\n",
    "\n",
    "* __iterator__: datastream returning member after member\n",
    "\n",
    "    * `__next__()`\n",
    "\n",
    "* __generator__: function yielding a series of objects\n",
    "\n",
    "    * `yield`\n",
    "\n",
    "Note:\n",
    "generators require less memory, iterable not saved in memory\n",
    "\n",
    "![iterables, iterators, generators, &#8230;](./images/itergen.png)\n",
    "\n",
    "Taken from [nvie.com](http://nvie.com/posts/iterators-vs-generators/)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### `range`\n",
    "\n",
    "* `range(start, stop, step)` from start to stop with stepsize `step` (stop excluded!)\n",
    "\n",
    "    * `range(5)` &sim; `[0, 1, 2, 3, 4]`\n",
    "    \n",
    "    * `range(2, 5)` &sim; `[2, 3, 4]`\n",
    "    \n",
    "    * `range(0, 5, 2)` &sim; `[0, 2, 4]`\n",
    "\n",
    "\n",
    "* `iterator`, not an `iterable`\n",
    "\n",
    "    * `range(5)` is an iterator\n",
    "    \n",
    "    * `list(range(5))` transforms iterator into iterable\n",
    "    \n",
    "\n",
    "**Note:** `range(5)` cannot be indexed (it is not an iterable!)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "r = range(10)             # this is an iterator\n",
    "it = r.__iter__()        \n",
    "print(it.__next__())       # getting elements, one by one \n",
    "print(it.__next__())\n",
    "print(it.__next__())\n",
    "\n",
    "while it:\n",
    "    print(it.__next__())   # generates a StopIteration exception when no more elements"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "r = range(10)       # this is an iterator\n",
    "\n",
    "it = r.__iter__()\n",
    "try:\n",
    "    while it:\n",
    "        print(it.__next__())\n",
    "except StopIteration:\n",
    "    pass\n",
    "finally:\n",
    "    print(\"no more elements in r\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### But wait! ... this time we've truly reinvented the for-loop"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for element in range(10):\n",
    "    print(element)\n",
    "print(\"no more elements in the iterator\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### list comprehension (very, very Pythonic)\n",
    "\n",
    "* constructing lists from iterators"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%timeit  # an ipython (hence jupyter) magic that allows to time execution of a cell\n",
    "y = list();\n",
    "for x in range(10000): \n",
    "    y.append(x ** 2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%timeit\n",
    "y = []\n",
    "for x in range(10000):\n",
    "    y += [x**2]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%timeit\n",
    "z = [x ** 2 for x in range(10000)]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* more involved including conditional statements (can you guess the output ?)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "w = [(x, y) for x in [1, 2, 3] for y in [1, 2, 3] if x < y]\n",
    "print(w)\n",
    "t = [(x, y) if x < y else (x, ) for x in [1, 2, 3] for y in [1, 2, 3]]\n",
    "print(t)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* also available for other containers\n",
    "  `x**2 for x in range(3)` is a generator and can be cast to a container"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print({x**2 for x in range(3)})\n",
    "print({x: x**2 for x in range(3)})\n",
    "print(tuple(x**2 for x in range(3)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3.5 `break`, `else`, `continue`, and `pass` <a name=\"break-cont-pass\"/>\n",
    "\n",
    "* `break`: breaks out of innermost `for`- or `while`-loop\n",
    "\n",
    "    * `else` executed if no break occurs\n",
    "\n",
    "\n",
    "* `continue`: continues with next iteration of loop\n",
    "\n",
    "\n",
    "* `pass`: does nothing (but syntactical necessity)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import random as rnd\n",
    "\n",
    "upper_bound = 1.1\n",
    "for x in range(10):\n",
    "    if rnd.uniform(0, upper_bound) > 1:\n",
    "        print('random number bigger than 1 found (in 10 trials)')\n",
    "        break\n",
    "else:\n",
    "    print('no random number greater than 1 found (in 10 trials)')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exercise: Prime sift <a name=\"primes\" />\n",
    "#### Give the number of prime numbers below 100 000\n",
    "_HINT_: use `for`, `break`, and `else` and a dynamically growing `list`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# ... to complete"
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.2"
  },
  "latex_envs": {
   "LaTeX_envs_menu_present": true,
   "autocomplete": true,
   "bibliofile": "biblio.bib",
   "cite_by": "apalike",
   "current_citInitial": 1,
   "eqLabelWithNumbers": true,
   "eqNumInitial": 1,
   "hotkeys": {
    "equation": "Ctrl-E",
    "itemize": "Ctrl-I"
   },
   "labels_anchors": false,
   "latex_user_defs": false,
   "report_style_numbering": false,
   "user_envs_cfg": false
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
